var gulp = require('gulp'),
    sass = require('gulp-sass'),
    autoprefixer = require('gulp-autoprefixer'),
    cleancss = require('gulp-clean-css'),
    rename = require('gulp-rename'),
    concat = require('gulp-concat'),
    uglify = require('gulp-uglify'),
    browserSync = require('browser-sync').create(),
    babel = require('gulp-babel'),
    del = require('del');

gulp.task('styles', function () {
    return gulp.src(['assets/scss/*.scss', 'assets/scss/lib/*.css'])
        .pipe(sass()).on('error', sass.logError)
        .pipe(autoprefixer({overrideBrowserslist: ['last 10 versions', 'ie >= 11', 'edge >= 14'], cascade: false}))
        .pipe(cleancss())
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/css/'))
        .pipe(browserSync.stream());
});

gulp.task('scripts', function () {
    return gulp
        .src("assets/js/*.js")
        .pipe(babel({presets: ["@babel/preset-env"]}))
        .pipe(uglify())
        .on("error", function (e) {
            console.log(e);
        })
        .pipe(rename({suffix: ".min"}))
        .pipe(gulp.dest("dist/js/"))
        .pipe(browserSync.stream());
});

gulp.task('page-scripts', function () {
    return gulp
        .src('assets/js/layout/*.js')
        .pipe(babel({presets: ['@babel/preset-env']}))
        .pipe(uglify()).on('error', function (e) {
            console.log(e);
        })
        .pipe(rename({suffix: '.min'}))
        .pipe(gulp.dest('dist/js/'));
});

gulp.task('libs', function () {
    return gulp
        .src('assets/libs/*.*')
        .pipe(gulp.dest('dist/libs/'))
        .pipe(browserSync.stream());
});

gulp.task('images', function () {
    return gulp
        .src('assets/images/*.*')
        .pipe(gulp.dest('dist/images/'))
        .pipe(browserSync.stream());
});

gulp.task('clean', async function () {
    return del.sync('dist');
});

gulp.task('watch', function () {
    browserSync.init({
        proxy: "diplom",
        notify: false
    });
    gulp.watch('assets/scss/**/*.scss', gulp.series('styles'));
    gulp.watch('assets/js/**', gulp.series('scripts', 'page-scripts'));
    gulp.watch('assets/images/*.*', gulp.series('images'));
    gulp.watch('*.php').on("change", browserSync.reload);

});

gulp.task('default', gulp.parallel('styles', 'scripts', 'page-scripts', 'libs', 'images', 'watch'));

gulp.task('build', gulp.series('clean', gulp.parallel('styles', 'scripts', 'page-scripts', 'libs', 'images')));
