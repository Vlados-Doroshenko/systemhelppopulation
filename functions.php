<?php
/**
 * diplom work functions and definitions
 *
 * @link    https://developer.wordpress.org/themes/basics/theme-functions/
 *
 * @package diplom_work
 */

if (!defined('_S_VERSION')) {
  // Replace the version number of the theme on each release.
  define('_S_VERSION', '1.0.0');
}

/**
 * Sets up theme defaults and registers support for various WordPress features.
 *
 * Note that this function is hooked into the after_setup_theme hook, which
 * runs before the init hook. The init hook is too late for some features, such
 * as indicating support for post thumbnails.
 */
function diplom_work_setup()
{
  /*
    * Make theme available for translation.
    * Translations can be filed in the /languages/ directory.
    * If you're building a theme based on diplom work, use a find and replace
    * to change 'diplom-work' to the name of your theme in all the template files.
    */
  load_theme_textdomain('diplom-work', get_template_directory() . '/languages');

  // Add default posts and comments RSS feed links to head.
  add_theme_support('automatic-feed-links');

  /*
    * Let WordPress manage the document title.
    * By adding theme support, we declare that this theme does not use a
    * hard-coded <title> tag in the document head, and expect WordPress to
    * provide it for us.
    */
  add_theme_support('title-tag');

  /*
    * Enable support for Post Thumbnails on posts and pages.
    *
    * @link https://developer.wordpress.org/themes/functionality/featured-images-post-thumbnails/
    */
  add_theme_support('post-thumbnails');

  // This theme uses wp_nav_menu() in one location.
  register_nav_menus(
      [
          'menu-1' => esc_html__('Header', 'diplom-work'),
      ]
  );

  /*
    * Switch default core markup for search form, comment form, and comments
    * to output valid HTML5.
    */
  add_theme_support(
      'html5',
      [
          'search-form',
          'comment-form',
          'comment-list',
          'gallery',
          'caption',
          'style',
          'script',
      ]
  );

  // Set up the WordPress core custom background feature.
  add_theme_support(
      'custom-background',
      apply_filters(
          'diplom_work_custom_background_args',
          [
              'default-color' => 'ffffff',
              'default-image' => '',
          ]
      )
  );

  // Add theme support for selective refresh for widgets.
  add_theme_support('customize-selective-refresh-widgets');

  /**
   * Add support for core custom logo.
   *
   * @link https://codex.wordpress.org/Theme_Logo
   */
  add_theme_support(
      'custom-logo',
      [
          'height'      => 250,
          'width'       => 250,
          'flex-width'  => true,
          'flex-height' => true,
      ]
  );
}

add_action('after_setup_theme', 'diplom_work_setup');

/**
 * Set the content width in pixels, based on the theme's design and stylesheet.
 *
 * Priority 0 to make it available to lower priority callbacks.
 *
 * @global int $content_width
 */
function diplom_work_content_width()
{
  $GLOBALS['content_width'] = apply_filters('diplom_work_content_width', 640);
}

add_action('after_setup_theme', 'diplom_work_content_width', 0);

/**
 * Register widget area.
 *
 * @link https://developer.wordpress.org/themes/functionality/sidebars/#registering-a-sidebar
 */
function diplom_work_widgets_init()
{
  register_sidebar(
      [
          'name'          => esc_html__('Sidebar', 'diplom-work'),
          'id'            => 'sidebar-1',
          'description'   => esc_html__('Add widgets here.', 'diplom-work'),
          'before_widget' => '<section id="%1$s" class="widget %2$s">',
          'after_widget'  => '</section>',
          'before_title'  => '<h2 class="widget-title">',
          'after_title'   => '</h2>',
      ]
  );
}

add_action('widgets_init', 'diplom_work_widgets_init');

if (function_exists('acf_add_options_page')) {

  acf_add_options_page([
      'page_title' => 'Theme General Settings',
      'menu_title' => 'Theme Settings',
      'menu_slug'  => 'theme-general-settings',
      'capability' => 'edit_posts',
      'redirect'   => false,
  ]);

  acf_add_options_sub_page([
      'page_title'  => 'Theme Header Settings',
      'menu_title'  => 'Header',
      'parent_slug' => 'theme-general-settings',
  ]);

  acf_add_options_sub_page([
      'page_title'  => 'Theme Footer Settings',
      'menu_title'  => 'Footer',
      'parent_slug' => 'theme-general-settings',
  ]);
}

/**
 * Enqueue scripts and styles.
 */
function diplom_work_scripts()
{
  wp_deregister_script('jquery');
  wp_enqueue_script('jquery', 'https://code.jquery.com/jquery-3.5.1.min.js', false, false, false);

  wp_enqueue_style('diplom-work-style', get_stylesheet_uri(), [], _S_VERSION);
  wp_enqueue_style('home-style', get_stylesheet_directory_uri() . '/dist/css/home.min.css', [], filemtime
  (get_theme_file_path('/dist/css/home.min.css')));
  wp_enqueue_style('jquery-modal-css', get_stylesheet_directory_uri() . '/dist/libs/jquery.modal.min.css');
  wp_enqueue_style('single-donation-style', get_stylesheet_directory_uri() . '/dist/css/single-donation.min.css', [],
      filemtime
      (get_theme_file_path('/dist/css/single-donation.min.css')));
  wp_enqueue_style('map-style', get_stylesheet_directory_uri()
                                . '/dist/css/map.min.css', [], filemtime(get_theme_file_path('/dist/css/map.min.css')));

  if (is_page('donation')) {
    wp_enqueue_style('donation-style', get_stylesheet_directory_uri() . '/dist/css/donation.min.css', [], filemtime
    (get_theme_file_path('/dist/css/donation.min.css')));
  }


  wp_enqueue_script('diplom-work-navigation', get_template_directory_uri() . '/js/navigation.js', [], _S_VERSION, true);
  wp_enqueue_script('jquery-modal-js', get_template_directory_uri()
                                       . '/dist/libs/jquery.modal.min.js', ['jquery'], _S_VERSION, true);
  wp_enqueue_script('header-js', get_template_directory_uri() . '/dist/js/header.min.js', ['jquery'], false, true);

  if (is_front_page()) {
    wp_enqueue_script('form-js', get_template_directory_uri() . '/dist/js/form-select.min.js', ['jquery'], false, true);
  }
}

add_action('wp_enqueue_scripts', 'diplom_work_scripts');
show_admin_bar(false);
function register_fund_raising()
{

  register_post_type('donation', [
      'label'         => null,
      'labels'        => [
          'name'               => 'Fund Raising',
          'singular_name'      => 'Fund Raising',
          'add_new'            => 'Add fund raising',
          'add_new_item'       => 'Add new fund raising',
          'edit_item'          => 'Edit fund raising',
          'new_item'           => '',
          'view_item'          => '',
          'search_items'       => '',
          'not_found'          => '',
          'not_found_in_trash' => '',
          'parent_item_colon'  => '',
          'menu_name'          => 'Fund Raising',
      ],
      'description'   => '',
      'public'        => true,
      'show_ui'       => true,
      'show_in_menu'  => null,
      'show_in_rest'  => null,
      'rest_base'     => null,
      'menu_position' => null,
      'menu_icon'     => null,
      'hierarchical'  => false,
      'supports'      => ['title', 'editor', 'thumbnail', 'excerpt'],
      'taxonomies'    => ['category', 'ammunition'],
      'has_archive'   => false,
      'rewrite'       => true,
      'query_var'     => true,
  ]);

}

add_action('init', 'register_fund_raising');

// хук для регистрации
add_action('init', 'create_taxonomy');
function create_taxonomy()
{

  // список параметров: wp-kama.ru/function/get_taxonomy_labels
  register_taxonomy('ammunition', ['donation'], [
      'label'        => '', // определяется параметром $labels->name
      'labels'       => [
          'name'              => 'Ammunition',
          'singular_name'     => 'Ammunition',
          'search_items'      => 'Search Ammunition',
          'all_items'         => 'All Ammunition',
          'view_item '        => 'View Ammunition',
          'parent_item'       => 'Parent Ammunition',
          'parent_item_colon' => 'Parent Ammunition:',
          'edit_item'         => 'Edit Ammunition',
          'update_item'       => 'Update Ammunition',
          'add_new_item'      => 'Add New Ammunition',
          'new_item_name'     => 'New Ammunition Name',
          'menu_name'         => 'Ammunition',
          'back_to_items'     => '← Back to Ammunition',
      ],
      'description'  => '', // описание таксономии
      'public'       => true,
    // 'publicly_queryable'    => null, // равен аргументу public
    // 'show_in_nav_menus'     => true, // равен аргументу public
    // 'show_ui'               => true, // равен аргументу public
    // 'show_in_menu'          => true, // равен аргументу show_ui
    // 'show_tagcloud'         => true, // равен аргументу show_ui
    // 'show_in_quick_edit'    => null, // равен аргументу show_ui
      'hierarchical' => false,

      'rewrite'           => true,
      'query_var'         => 'ammunition', // название параметра запроса
      'capabilities'      => [],
      'meta_box_cb'       => 'post_categories_meta_box', // html метабокса. callback: `post_categories_meta_box` или
    // `post_tags_meta_box`. false — метабокс отключен.
      'show_admin_column' => true,                       // авто-создание колонки таксы в таблице ассоциированного типа записи. (с версии
    // 3.5)
      'show_in_rest'      => null,                       // добавить в REST API
      'rest_base'         => null,                       // $taxonomy
    // '_builtin'              => false,
    //'update_count_callback' => '_update_post_term_count',
  ]);
}

function posts_filters()
{
  $args = [
      'post_type' => 'donation',
      'tax_query' => [
          'relation' => 'AND',
      ],
  ];

  if (isset($_POST['categoryfilter'])) {
    $args['tax_query'] = [
        [
            'taxonomy' => 'category',
            'field'    => 'id',
            'terms'    => $_POST['categoryfilter'],
        ],
    ];
  }

  if (isset($_POST['tagfilter'])) {
    $args['tax_query'] = [
        [
            'taxonomy' => 'ammunition',
            'field'    => 'id',
            'terms'    => $_POST['tagfilter'],
        ],
    ];
  }

  $query = new WP_Query($args);

  if ($query->have_posts()) :
    while ($query->have_posts()) {
      $query->the_post();
      ?>
      <a class="donation__items" href="<?php the_permalink(); ?>">
        <?php if (has_post_thumbnail()) {
          the_post_thumbnail('post_thumb');
        } else { ?>
          <img src="<?= get_template_directory_uri() . '/dist/images/no-photo.jpg'; ?>" alt="No Photo"/>
        <?php } ?>
        <div class="donation__items__content">
          <h2 class="content__title"><?php the_title(); ?></h2>
          <!--                <svg width="44" height="32" viewBox="0 0 44 32" fill="none" xmlns="http://www.w3.org/2000/svg">-->
          <!--                  <circle cx="28" cy="16" r="16" fill="#7A87FF"/>-->
          <!--                  <path-->
          <!--                      d="M32.7071 16.7071C33.0976 16.3166 33.0976 15.6834 32.7071 15.2929L26.3431 8.92893C25.9526 8.53841 25.3195 8.53841 24.9289 8.92893C24.5384 9.31946 24.5384 9.95262 24.9289 10.3431L30.5858 16L24.9289 21.6569C24.5384 22.0474 24.5384 22.6805 24.9289 23.0711C25.3195 23.4616 25.9526 23.4616 26.3431 23.0711L32.7071 16.7071ZM0 17H32V15H0V17Z"-->
          <!--                      fill="white"/>-->
          <!--                </svg>-->

          <div class="content__description">
            <?php the_excerpt(); ?>
          </div>

          <div class="content__taxonomy">
            <div class="content__location">
              <?php if (get_the_category()) : ?>
                <div class="">
                  <?php foreach (get_the_category() as $name_categ) : ?>
                    <p><?= $name_categ->cat_name; ?></p>
                  <?php endforeach; ?>
                </div>
              <?php endif;

              if (get_the_terms(get_the_ID(), 'ammunition')) : ?>
                <div class="">
                  <?php foreach (get_the_terms(get_the_ID(), 'ammunition') as $name_tag) :
                    if (!is_null($name_tag->name)) :
                      ?>
                      <p><?= $name_tag->name; ?></p>
                    <?php endif;

                  endforeach; ?>
                </div>
              <?php endif; ?>
            </div>

            <div class="">
              <?php the_date('d.m.Y'); ?>
            </div>
          </div>
        </div>
      </a>
    <?php }
    wp_reset_postdata();
  else :
    echo 'Записів не знайдено';
  endif;

  die();
}

add_action('wp_ajax_customfilter', 'posts_filters');
add_action('wp_ajax_nopriv_customfilter', 'posts_filters');

/**
 * Implement the Custom Header feature.
 */
require get_template_directory() . '/inc/custom-header.php';

/**
 * Custom template tags for this theme.
 */
require get_template_directory() . '/inc/template-tags.php';

/**
 * Functions which enhance the theme by hooking into WordPress.
 */
require get_template_directory() . '/inc/template-functions.php';

/**
 * Customizer additions.
 */
require get_template_directory() . '/inc/customizer.php';

/**
 * Load Jetpack compatibility file.
 */
if (defined('JETPACK__VERSION')) {
  require get_template_directory() . '/inc/jetpack.php';
}
