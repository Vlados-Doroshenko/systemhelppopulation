<?php
/**
 * The header for our theme
 *
 * This is the template that displays all of the <head> section and everything up until <div id="content">
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package diplom_work
 */

$location_data = geoip_detect2_get_info_from_current_ip()->raw;
$location      = $location_data['location'];
$longitude     = $location['longitude'];
$latitude      = $location['latitude'];
$timezone      = $location['time_zone'];

$s = 'https://ipapi.co/' . $location_data['traits']['ip_address'] . '/xml';
$x = simplexml_load_string(file_get_contents($s));
?>
<!doctype html>
<html <?php language_attributes(); ?>>
<head>
  <meta charset="<?php bloginfo('charset'); ?>">
  <meta name="viewport" content="width=device-width, initial-scale=1">
  <link rel="profile" href="https://gmpg.org/xfn/11">
  <link rel="stylesheet" type="text/css"
        href="https://map.ukrainealarm.com/css/svgMap.css?v=qR5vILr_Ue_bbD-chpoRfPgxoKVtVjbv-jrACgwfH8Y">

  <?php wp_head(); ?>
</head>

<body <?php body_class(); ?>>
<?php wp_body_open(); ?>
<div id="page" class="site">
  <header id="header" class="site-header header">
    <div class="container">
      <div class="header__wrapper">
        <div class="site-branding">
          <?php the_custom_logo(); ?>
        </div><!-- .site-branding -->

        <nav id="site-navigation" class="main-navigation">
          <?php
          wp_nav_menu(
              [
                  'theme_location' => 'menu-1',
                  'menu_id'        => 'primary-menu',
              ]
          );
          ?>
        </nav><!-- #site-navigation -->

        <div class="header__message">
          <div class="header__login">
            <a href="<?= site_url() . '/wp-admin'; ?>">
              <img src="<?= get_template_directory_uri() . '/dist/images/login.png'; ?>" alt="Icon login"/>
            </a>
          </div>

          <div class="header__location">
            <img src="<?= get_template_directory_uri() . '/dist/images/location.png'; ?>" alt="Icon Location"/>
            <p>
              <?= $x->city; ?>
            </p>
          </div>

          <button id="switch-alarm" class="btn-alarm alarm-on">
            <svg xmlns="http://www.w3.org/2000/svg" version="1.0" width="50" height="50" viewBox="0 0 75 75">
              <path d="M39.389,13.769 L22.235,28.606 L6,28.606 L6,47.699 L21.989,47.699 L39.389,62.75 L39.389,13.769z"
                    style="stroke:#111;stroke-width:2;stroke-linejoin:round;fill:green;"
              />
              <path d="M48,27.6a19.5,19.5 0 0 1 0,21.4M55.1,20.5a30,30 0 0 1 0,35.6M61.6,14a38.8,38.8 0 0 1 0,48.6"
                    style="fill:none;stroke:green;stroke-width:2;stroke-linecap:round"/>
            </svg>
          </button>
        </div>
      </div>
      <div class="mobile">
        <hr>

        <button id="header_btn" class="mobile__menu">
          <svg width="40px" height="40px" viewBox="0 0 72 72" id="emoji" xmlns="http://www.w3.org/2000/svg">
            <g id="color"/>
            <g id="hair"/>
            <g id="skin"/>
            <g id="skin-shadow"/>
            <g id="line">
              <line x1="16" x2="56" y1="26" y2="26" fill="none" stroke="#fff" stroke-linecap="round"
                    stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
              <line x1="16" x2="56" y1="36" y2="36" fill="none" stroke="#fff" stroke-linecap="round"
                    stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
              <line x1="16" x2="56" y1="46" y2="46" fill="none" stroke="#fff" stroke-linecap="round"
                    stroke-linejoin="round" stroke-miterlimit="10" stroke-width="2"/>
            </g>
          </svg>
        </button>

        <div id="header_side_menu" class="mobile__list disActive">
          <nav id="site-navigation" class="main-navigation">
            <?php
            wp_nav_menu(
                [
                    'theme_location' => 'menu-1',
                    'menu_id'        => 'primary-menu',
                ]
            );
            ?>
          </nav>
        </div>
      </div>
    </div>
  </header><!-- #masthead -->
