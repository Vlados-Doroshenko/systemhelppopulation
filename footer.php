<?php
/**
 * The template for displaying the footer
 *
 * Contains the closing of the #content div and all content after.
 *
 * @link    https://developer.wordpress.org/themes/basics/template-files/#template-partials
 *
 * @package diplom_work
 */

?>

<footer id="footer" class="site-footer">
  <div class="site-info">
    <div class="footer__copyright">
      <p>
        Copyright © Vlad Doroshenko 2023
      </p>
    </div>
  </div><!-- .site-info -->
</footer><!-- #colophon -->

<?php include 'template-page/popup.php'; ?>

</div><!-- #page -->

<?php wp_footer(); ?>

</body>
</html>
