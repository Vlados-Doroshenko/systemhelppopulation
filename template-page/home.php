<?php
/*
 * Template Name: Home Template
 */


get_header();
?>

  <main id="homepage" class="wrapper">
    <section class="hero">
      <div class="container">
        <div class="hero__wrapper">
          <h1 class="hero__title">
            <?= get_field('hero_title'); ?>
          </h1>

          <p class="hero__text">
            <?= get_field('hero_subtitle'); ?>
          </p>

          <div class="button button_group">
            <a href="#contact">Хочу волонтерити!</a>
            <a href="#contact">Шукаю волонтерів</a>
          </div>

          <a href="#help" class="hero__arrow">
            <img src="<?= get_template_directory_uri() . '/dist/images/arrow-down.gif'; ?>" alt="Arrow Down"/>
          </a>
        </div>
      </div>
    </section>

    <section id="help" class="help">
      <div class="container">
        <div class="help__wrapper">
          <div class="help__content">
            <h2 class="help__title">
              <?= get_field('about_title'); ?>
            </h2>

            <div class="help__text">
              <span>❗️</span><br>
              <?= get_field('about_text'); ?>
            </div>
          </div>

          <div class="button">
            <a href="#contact">Стати Волонтером</a>
          </div>
        </div>
      </div>
    </section>

    <section class="about-us">
      <div class="container">
        <h2 class="title">
          <?= get_field('help_title'); ?>
        </h2>

        <p class="subtitle">
          <?= get_field('help_subtitle'); ?>
        </p>

        <div class="about-us__wrapper">
          <div class="about-us__content">
            <h3 class="about-us__title">
              <?= get_field('help_body_title'); ?>
            </h3>

            <?= get_field('help_content'); ?>
          </div>

          <div class="about-us__content">
            <h3 class="about-us__title">
              <?= get_field('help_title_work'); ?>
            </h3>

            <div class="about-us__group group">
              <?php
              if (get_field('help_work')):
                foreach (get_field('help_work') as $item):
                  ?>
                  <div class="group__wrapper">
                    <div class="group__img">
                      <img src="<?= $item['help_image']; ?>" alt="Good"/>
                    </div>

                    <div class="group__content">
                      <h4 class="group__title">
                        <?= $item['help_title_work']; ?>
                      </h4>

                      <?= $item['help_text_work']; ?>
                    </div>
                  </div>
                <?php endforeach;

              endif; ?>
            </div>
          </div>
        </div>
      </div>
    </section>

    <section id="donation" class="donation">
      <div class="container">
        <h2 class="title">
          Збір коштів
        </h2>

        <div class="donation__wrapper">
          <?php
          $donation_array = new WP_Query([
              'post_type'      => 'donation',
              'posts_per_page' => 8,
              'order'          => 'DESC',
          ]);

          while ($donation_array->have_posts()) {
            $donation_array->the_post();
            ?>
            <a class="donation__items" href="<?php the_permalink(); ?>">
              <?php if (has_post_thumbnail()) {
                the_post_thumbnail('post_thumb');
              } else { ?>
                <img src="<?= get_template_directory_uri() . '/dist/images/no-photo.jpg'; ?>" alt="No Photo"/>
              <?php } ?>
              <div class="donation__items__content">
                <h2 class="content__title"><?php the_title(); ?></h2>
                <div class="content__description">
                  <?php the_excerpt(); ?>
                </div>

                <div class="content__taxonomy">
                  <div class="content__location">
                    <?php if (get_the_category()) : ?>
                      <div class="">
                        <?php foreach (get_the_category() as $name_categ) : ?>
                          <p><?= $name_categ->cat_name; ?></p>
                        <?php endforeach; ?>
                      </div>
                    <?php endif;

                    if (get_the_terms(get_the_ID(), 'ammunition')) : ?>
                      <div class="">
                        <?php foreach (get_the_terms(get_the_ID(), 'ammunition') as $name_tag) :
                          if (!is_null($name_tag->name)) :
                            ?>
                            <p><?= $name_tag->name; ?></p>
                          <?php endif;

                        endforeach; ?>
                      </div>
                    <?php endif; ?>
                  </div>

                  <div class="">
                    <?php the_time('d.m.Y'); ?>
                  </div>
                </div>
              </div>
            </a>
          <?php }

          wp_reset_postdata();
          ?>
        </div>
        <a href="/donation" class="donation__btn">
          Переглянути всі
        </a>
      </div>
    </section>

    <section id="contact" class="contact">
      <div class="container">
        <h2 class="title">
          <?= get_field('contact_title'); ?>
        </h2>

        <div class="subtitle">
          <?= get_field('contact_subtitle_one'); ?>
        </div>

        <div class="subtitle">
          <span>❗</span><br>
          <?= get_field('contact_subtitle_two'); ?>
        </div>

        <div class="contact__wrapper">
          <?= do_shortcode('[contact-form-7 id="9" title="Contact"]'); ?>
        </div>
      </div>
    </section>
  </main>
<?php
get_footer();