<?php
/**
 * The template for displaying all single help posts
 *
 *
 * @package diplom_work
 */

get_header();
?>

  <main id="single-help" class="wrapper">
    <section class="help">
      <?php
      while (have_posts()) :
        the_post();
        ?>
        <div class="container">
          <div class="help__wrapper">
            <div class="help__wrapper__items help__content">
              <h1 class="help__content__title">
                <?php the_title(); ?>
              </h1>

              <div class="help__wrapper__content">
                <?php if (has_post_thumbnail()) {
                  the_post_thumbnail('post_thumb');
                } else { ?>
                  <img src="<?= get_template_directory_uri() . '/dist/images/no-photo.jpg'; ?>" alt="No Photo"/>
                <?php } ?>

                <div class="help__content__description">
                  <?php the_content(); ?>
                </div>

                <?php if (get_field('donation') && get_field('donation_image')): ?>
                  <div class="help__content__cart">
                    <a href="<?= get_field('donation'); ?>" target="_blank">
                      <img src="<?= get_field('donation_image'); ?>" alt="Donation">
                    </a>
                  </div>
                <?php endif; ?>
              </div>
            </div>
          </div>
        </div>
      <?php
      endwhile; // End of the loop.
      ?>
    </section>
  </main>

<?php
get_footer();
