(function ($) {
    $(document).ready(function () {
        const cities = `{
            "data": {
                "areas": [
                    {
                        "title": "Вінницька область",
                        "id": 1,
                        "square": 26513.0,
                        "population": 1546620,
                        "local_community_count": 707,
                        "percent_communities_from_area": 21.01,
                        "sum_communities_square": 5570.2
                    },
                    {
                        "title": "Волинська область",
                        "id": 2,
                        "square": 20144.0,
                        "population": 1031821,
                        "local_community_count": 412,
                        "percent_communities_from_area": 58.59,
                        "sum_communities_square": 11802.08
                    },
                    {
                        "title": "Дніпропетровська область",
                        "id": 3,
                        "square": 31914.0,
                        "population": 3179008,
                        "local_community_count": 348,
                        "percent_communities_from_area": 66.61,
                        "sum_communities_square": 21258.65
                    },
                    {
                        "title": "Донецька область",
                        "id": 9,
                        "square": 26517.0,
                        "population": 4134399,
                        "local_community_count": 384,
                        "percent_communities_from_area": 19.74,
                        "sum_communities_square": 5233.89
                    },
                    {
                        "title": "Житомирська область",
                        "id": 10,
                        "square": 29832.0,
                        "population": 1209272,
                        "local_community_count": 632,
                        "percent_communities_from_area": 65.6,
                        "sum_communities_square": 19570.014
                    },
                    {
                        "title": "Закарпатська область",
                        "id": 4,
                        "square": 12777.0,
                        "population": 1254267,
                        "local_community_count": 337,
                        "percent_communities_from_area": 13.21,
                        "sum_communities_square": 1687.65
                    },
                    {
                        "title": "Запорізька область",
                        "id": 5,
                        "square": 27180.0,
                        "population": 1688829,
                        "local_community_count": 299,
                        "percent_communities_from_area": 68.83,
                        "sum_communities_square": 18707.34
                    },
                    {
                        "title": "Івано-Франківська область",
                        "id": 6,
                        "square": 13900.0,
                        "population": 1368440,
                        "local_community_count": 516,
                        "percent_communities_from_area": 29.06,
                        "sum_communities_square": 4039.993
                    },
                    {
                        "title": "Київська область",
                        "id": 11,
                        "square": 28131.0,
                        "population": 1779704,
                        "local_community_count": 661,
                        "percent_communities_from_area": 18.62,
                        "sum_communities_square": 5236.868
                    },
                    {
                        "title": "Кіровоградська область",
                        "id": 7,
                        "square": 24588.0,
                        "population": 934021,
                        "local_community_count": 415,
                        "percent_communities_from_area": 24.61,
                        "sum_communities_square": 6050.35
                    },
                    {
                        "title": "Луганська область",
                        "id": 12,
                        "square": 26684.0,
                        "population": 2137181,
                        "local_community_count": 332,
                        "percent_communities_from_area": 26.37,
                        "sum_communities_square": 7037.72
                    },
                    {
                        "title": "Львівська область",
                        "id": 13,
                        "square": 21833.0,
                        "population": 2513007,
                        "local_community_count": 711,
                        "percent_communities_from_area": 23.28,
                        "sum_communities_square": 5082.88
                    },
                    {
                        "title": "Миколаївська область",
                        "id": 14,
                        "square": 24598.0,
                        "population": 1120789,
                        "local_community_count": 313,
                        "percent_communities_from_area": 51.22,
                        "sum_communities_square": 12600.01
                    },
                    {
                        "title": "Одеська область",
                        "id": 15,
                        "square": 33310.0,
                        "population": 2377037,
                        "local_community_count": 490,
                        "percent_communities_from_area": 34.69,
                        "sum_communities_square": 11555.999
                    },
                    {
                        "title": "Полтавська область",
                        "id": 16,
                        "square": 28748.0,
                        "population": 1388184,
                        "local_community_count": 503,
                        "percent_communities_from_area": 34.96,
                        "sum_communities_square": 10051.4
                    },
                    {
                        "title": "Рівненська область",
                        "id": 17,
                        "square": 20047.0,
                        "population": 1153514,
                        "local_community_count": 365,
                        "percent_communities_from_area": 39.28,
                        "sum_communities_square": 7874.42
                    },
                    {
                        "title": "Сумська область",
                        "id": 18,
                        "square": 23834.0,
                        "population": 1069138,
                        "local_community_count": 419,
                        "percent_communities_from_area": 43.54,
                        "sum_communities_square": 10376.2
                    },
                    {
                        "title": "Тернопільська область",
                        "id": 19,
                        "square": 13823.0,
                        "population": 1039219,
                        "local_community_count": 615,
                        "percent_communities_from_area": 49.2,
                        "sum_communities_square": 6800.94
                    },
                    {
                        "title": "Харківська область",
                        "id": 20,
                        "square": 31415.0,
                        "population": 2659937,
                        "local_community_count": 458,
                        "percent_communities_from_area": 24.16,
                        "sum_communities_square": 7588.373
                    },
                    {
                        "title": "Херсонська область",
                        "id": 21,
                        "square": 28461.0,
                        "population": 1028830,
                        "local_community_count": 298,
                        "percent_communities_from_area": 33.96,
                        "sum_communities_square": 9665.54
                    },
                    {
                        "title": "Хмельницька область",
                        "id": 22,
                        "square": 20645.0,
                        "population": 1255522,
                        "local_community_count": 605,
                        "percent_communities_from_area": 61.25,
                        "sum_communities_square": 12645.852
                    },
                    {
                        "title": "Черкаська область",
                        "id": 23,
                        "square": 20900.0,
                        "population": 1193275,
                        "local_community_count": 556,
                        "percent_communities_from_area": 40.8,
                        "sum_communities_square": 8527.59
                    },
                    {
                        "title": "Чернівецька область",
                        "id": 24,
                        "square": 8097.0,
                        "population": 901910,
                        "local_community_count": 271,
                        "percent_communities_from_area": 47.92,
                        "sum_communities_square": 3879.71
                    },
                    {
                        "title": "Чернігівська область",
                        "id": 25,
                        "square": 31865.0,
                        "population": 992468,
                        "local_community_count": 568,
                        "percent_communities_from_area": 64.55,
                        "sum_communities_square": 20570.16
                    },
                    {
                        "title": "місто Київ",
                        "id": 28,
                        "square": 847.0,
                        "population": 2963199,
                        "local_community_count": 1,
                        "percent_communities_from_area": null,
                        "sum_communities_square": null
                    }
                ]
            }
        }`;

        const formSelect = document.getElementById('form-select');

        let city = JSON.parse(cities);

        city.data.areas.map(item => {
            const newOption = document.createElement("option");

            newOption.setAttribute('value', item.title);
            newOption.text = item.title;
            formSelect.appendChild(newOption);
        });
    });
})($);