(function ($) {
    $(document).ready(function () {
        const menuBtn = document.getElementById('header_btn');
        const headerSideMenu = document.getElementById('header_side_menu');
        const stopScrollBody = document.querySelector('body');

        menuBtn.addEventListener('click', () => {
            if (!headerSideMenu.classList.contains('active')) {
                headerSideMenu.classList.add('active');
                headerSideMenu.classList.remove('disActive');
                stopScrollBody.style.overflow = 'hidden';
            } else {
                headerSideMenu.classList.remove('active');
                headerSideMenu.classList.add('disActive');
                stopScrollBody.removeAttribute("style");
            }
        });

        $(window).on('resize', function () {
            if ($(window).width() >= 860) {
                headerSideMenu.classList.remove('active');
                headerSideMenu.classList.add('disActive');
            }
        });
    });
})($);