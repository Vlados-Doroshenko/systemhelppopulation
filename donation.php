<?php
/*
 * Template Name: Donation Template
 */


get_header();
?>

  <main id="donation" class="wrapper">
    <section class="hero">
      <div class="container">
        <div class="hero__wrapper">
          <h1 class="title-section">
            Збір коштів
          </h1>
        </div>
      </div>
    </section>

    <section class="donation">
      <div class="container">
        <div class="donation__wrapper__single">
          <div class="donation__sidebar">
            <div class="sidebar">
              <h2>Фільтр</h2>

              <div class="sidebar__wrapper">
                <form action="<?php echo site_url() ?>/wp-admin/admin-ajax.php" method="POST" id="post-date-filter">
                  <?php
                  if ($categories = get_terms('category')) :
                    foreach ($categories as $category) : ?>
                      <div class="">
                        <input type="checkbox" name="categoryfilter[]" id="category_<?= $category->term_id; ?>"
                               value="<?= $category->term_id; ?>"/>
                        <label for="category_<?= $category->term_id; ?>"><?= $category->name . ' (' . $category->count
                                                                             . ')';
                          ?></label>
                      </div>
                    <?php endforeach;

                  endif; ?>

                  <?php if ($post_tags = get_terms('ammunition')): ?>
                    <hr>

                    <?php foreach ($post_tags as $tag) : ?>
                      <div class="">
                        <input type="checkbox" name="tagfilter[]" id="post_tag_<?= $tag->term_id; ?>"
                               value="<?= $tag->term_id; ?>"/>
                        <label for="post_tag_<?= $tag->term_id; ?>"><?= $tag->name . ' (' . $tag->count
                                                                        . ')'; ?></label>
                      </div>
                    <?php endforeach;

                  endif; ?>

                  <button>Застосувати фільтр</button>
                  <input type="hidden" name="action" value="customfilter">
                </form>
              </div>
            </div>
          </div>
          <div class="donation__wrapper">
            <?php
            $donation_array = new WP_Query([
                'post_type' => 'donation',
            ]);

            while ($donation_array->have_posts()) {
              $donation_array->the_post();
              ?>
              <a class="donation__items" href="<?php the_permalink(); ?>">
                <?php if (has_post_thumbnail()) {
                  the_post_thumbnail('post_thumb');
                } else { ?>
                  <img src="<?= get_template_directory_uri() . '/dist/images/no-photo.jpg'; ?>" alt="No Photo"/>
                <?php } ?>
                <div class="donation__items__content">
                  <h2 class="content__title"><?php the_title(); ?></h2>
                  <!--                <svg width="44" height="32" viewBox="0 0 44 32" fill="none" xmlns="http://www.w3.org/2000/svg">-->
                  <!--                  <circle cx="28" cy="16" r="16" fill="#7A87FF"/>-->
                  <!--                  <path-->
                  <!--                      d="M32.7071 16.7071C33.0976 16.3166 33.0976 15.6834 32.7071 15.2929L26.3431 8.92893C25.9526 8.53841 25.3195 8.53841 24.9289 8.92893C24.5384 9.31946 24.5384 9.95262 24.9289 10.3431L30.5858 16L24.9289 21.6569C24.5384 22.0474 24.5384 22.6805 24.9289 23.0711C25.3195 23.4616 25.9526 23.4616 26.3431 23.0711L32.7071 16.7071ZM0 17H32V15H0V17Z"-->
                  <!--                      fill="white"/>-->
                  <!--                </svg>-->

                  <div class="content__description">
                    <?php the_excerpt(); ?>
                  </div>

                  <div class="content__taxonomy">
                    <div class="content__location">
                      <?php if (get_the_category()) : ?>
                        <div class="">
                          <?php foreach (get_the_category() as $name_categ) : ?>
                            <p><?= $name_categ->cat_name; ?></p>
                          <?php endforeach; ?>
                        </div>
                      <?php endif;

                      if (get_the_terms(get_the_ID(), 'ammunition')) : ?>
                        <div class="">
                          <?php foreach (get_the_terms(get_the_ID(), 'ammunition') as $name_tag) :
                            if (!is_null($name_tag->name)) :
                              ?>
                              <p><?= $name_tag->name; ?></p>
                            <?php endif;

                          endforeach; ?>
                        </div>
                      <?php endif; ?>
                    </div>

                    <div class="">
                      <?php the_time('d.m.Y'); ?>
                    </div>
                  </div>
                </div>
              </a>
            <?php } ?>
          </div>
        </div>
      </div>
    </section>
  </main>

  <script>
      jQuery(function ($) {
          $('#post-date-filter').submit(function () {
              const filter = $('#post-date-filter');
              $.ajax({
                  url: filter.attr('action'),
                  data: filter.serialize(),
                  type: filter.attr('method'),
                  beforeSend: function (xhr) {
                      filter.find('button').text('Застосовуємо фільтр...');
                  },
                  success: function (data) {
                      filter.find('button').text('Застосувати фільтр');
                      $('.donation__wrapper').html(data);
                  }
              });
              return false;
          });
      });
  </script>

<?php
get_footer();